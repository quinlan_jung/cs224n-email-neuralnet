import email
import os
import re

ORIGINAL_BOUNDARY = 'Original Message'
ORIGINAL_HEADERS = ['From:', 'Sent:', 'To:', 'Subject:', 'Cc:', 'cc:']
QUOTE_EMAIL_REGEX = re.compile('.*\".*\" <.*@.*> on .*') # "Eva Pao" <epao@mba2002.hbs.edu> on 05/09/2001 05:06:50 PM
ATTACHMENT = re.compile(".*<<.*>>.*")
def cleanMsg(msg):
    msg = msg.split('\n')
    msg = [m for m in msg if not any(m.startswith(hdr) for hdr in ORIGINAL_HEADERS)]
    msg = [m for m in msg if len(m) != 0]
    msg = [m for m in msg if not isQuoteEmail(m)]
    msg = [m for m in msg if not isAttachment(m)]
    return '\n'.join(msg)

def isQuoteEmail(line):
    return QUOTE_EMAIL_REGEX.search(line) is not None 

def isAttachment(line):
    return ATTACHMENT.search(line) is not None

def parseEmail(filename):
    mail = open(filename, 'r').read()
    mail = email.message_from_string(mail)
    mail = mail.get_payload()
    mail = mail.split('\n')
    mail = [m.lstrip('>') for m in mail]
    mail = [m.lstrip() for m in mail]
    mail = [m.lstrip('-') for m in mail]
    mail = [m.lstrip() for m in mail]
    mail = [m.rstrip() for m in mail]
    mail = [m.rstrip('-') for m in mail]
    mail = [m.rstrip() for m in mail]
    mail = '\n'.join(mail)
    mail = mail.split(ORIGINAL_BOUNDARY)

    if len(mail) < 2:
        return None, None

    mail = [cleanMsg(i) for i in mail]

    if len(mail[0]) == 0:
        return None, None

    return mail[0], '\n'.join(mail[1:])

def writeToFile(content, filename):
    file = open(filename, 'w')
    file.write(content) 

def crawlEmailDir(dirName):
    for root, dirs, files in os.walk(dirName, topdown=False):
        for name in files:
            out, context = parseEmail(os.path.join(root, name))
            if out is None:
                continue
            person = '-'.join(os.path.join(root, name).split('/')[4:]) # HACKY
            outFile = os.path.join('out', person + '.out')
            contextFile = os.path.join('out', person + '.context')
            writeToFile(out, outFile)
            writeToFile(context, contextFile)

crawlEmailDir('/Volumes/My Passport/maildir/')
#crawlEmailDir('/Users/quin/Google Drive/CS224n/finalproj/test_in/')


