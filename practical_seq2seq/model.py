#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Seq2Seq RNN model
"""

import tensorflow as tf
import numpy as np
import params  
import pickle
import time 
import random
from dataPreprocess import *

def pad_sequences(contID, resID, maxLen):
    """Ensures each contex is of length @maxLenCont and each response is
    of @maxLenRes by padding it with zeros.

    In the code below, for every sentence in @contID and @resID is appended with 
    zero feature vectors until the sentence is of length @maxLen, the maximum 
    sentence length of contex and response.   In addiction, a _masking_ sequence 
    that has a True wherever there was a token in the response sequence, and 
    a False for every padded input is generated for the masking in the loss 
    function.

    Example: for the (contex, response) pair: [4, 6, 7], [1,
    0, 0], and max_length = 5, we would construct
        - a new sentence: [4, 6, 7, 0, 0]
        - a new label seqeunce: [1, 0, 0, 0, 0], and
        - a masking seqeunce: [True, True, True, False, False].

    Args:
        contID: the contex data in word id sequence 
	resID: the response data in word id sequence 
  	maxLen: the maximum length of all response and contex sequence 
    Returns:
        a new list of data points of the structure (sentence, labels, mask).
        (the mask is the same langth as labels)
    """
    ret = []

    # Use this zero vector when padding sequences.
    zero_vector = PAD_index 
    zero_label = PAD_index # corresponds to the 'O' tag
    numData = len(contID)
    contID_padded = []
    resID_padded = []
    masks = []
    for i in range(numData):
        sentence = contID[i]
	label = resID[i]
  	sentenceLen = len(sentence)
	labelLen = len(label)
        padSentence = [sentence[i] if i < sentenceLen else zero_vector for i in range(maxLen)]
        padLabels = [label[i] if i < labelLen else zero_label for i in range(maxLen) ]
        mask = [True if i < labelLen else False for i in range(maxLen)]
        contID_padded.append(padSentence)
        resID_padded.append(padLabels)
        masks.append(mask)
    return contID_padded, resID_padded, masks

def test_pad_sequences():
    contID = [[4, 6, 7, 1, 0, 0],
              [3, 4, 5, 5, 3, 3, 10, 21]]
    resID = [[3, 51, 9, 4],
	     [7, 30, 11, 4, 9]]
    contID_padded = [[4, 6, 7, 1, 0, 0, PAD_index, PAD_index],[3, 4, 5, 5, 3, 3, 10, 21]]
    resID_padded = [[3, 51, 9, 4, PAD_index, PAD_index, PAD_index, PAD_index],[7, 30, 11, 4, 9, PAD_index,PAD_index,PAD_index]]
    masks = [[True, True, True,True, False, False, False, False],[True, True, True, True, True, False, False, False]]
    contPad, resPad, masksPad = pad_sequences(contID, resID, 8)
    if contPad != contID_padded: print "Contex padding failed!"
    if resPad != resID_padded: print "Response padding failed!"
    if masksPad != masks: print "Mask padding failed!"
    print("pad sequences test passed!")

def load_pretrained_embed():
    f = open('pretrained_embed.pckl', 'rb')
    embed = pickle.load(f)
    f.close()
    return embed

def load_vocab_dict():
    f = open('vocab.pckl','rb')
    vocab = pickle.load(f)
    f.close()
    return vocab

def load_test_data():
    contID = []
    resID = []
    maxLenCont = 34
    maxLenRes = 21
    numTrainExample = 32 #equals the batch size
    for i in range(numTrainExample):
        contLen = np.random.randint(1,maxLenCont)
	resLen = np.random.randint(1,maxLenRes)
	contID.append(random.sample(xrange(100),contLen))
	resID.append(random.sample(xrange(100),resLen)) 
    contID_pad, resID_pad, masks = pad_sequences(contID,resID,max(maxLenCont,maxLenRes))
    return (contID_pad, resID_pad, masks)
 
"""
This function loads numBatches test data where 
each batch is a tuple of (contID_pad, resID_pad, masks)
@numBatches: the number of batches wants to generate 
@return: a list of tuple of (contID_pad, resID_pad, masks)
"""
def load_test_batches(numBatches):
    return [load_test_data() for i in range(numBatches)]

def testRNNModel():
    print("Testing implementation of RNNModel...")
    testMaxSeqLen = 34
    config = Config(testMaxSeqLen)
    numTestBatches = 5
    batches  = load_test_batches(numTestBatches)
    embeddings = load_pretrained_embed()
    
    with tf.Graph().as_default():
        print "Building model..."
        start = time.time()
        model = RNNModel(config, embeddings)
        print "took ", time.time() - start, "seconds"

        init = tf.global_variables_initializer()
        saver = tf.train.Saver()

        with tf.Session() as session:
            session.run(init)
	    for batch in batches:
      		inputs = batch[0]
  		labels = batch[1]
		masks = batch[2] 
        	print "Training lost: ", model.train_on_batch(session, inputs, labels, masks)
	    saver.save(session,model.config.model_output)
    print("Model did not crash!")
    print("Passed!")

#==========RNN Model================#
class RNNModel:
    """
    Implements a recursive neural network with an embedding layer and
    single hidden layer.
    This network will predict a sequence of labels (e.g. PER) for a
    given token (e.g. Henry) using a featurized window around the token.
    """

    def add_placeholders(self):
        """Generates placeholder variables to represent the input tensors

        These placeholders are used as inputs by the rest of the model building and will be fed
        data during training.  Note that when "None" is in a placeholder's shape, it's flexible
        (so we can use different batch sizes without rebuilding the model).

        Adds following nodes to the computational graph

        input_placeholder: Input placeholder tensor of  shape (None, self.max_length, n_features), type tf.int32
        labels_placeholder: Labels placeholder tensor of shape (None, self.max_length), type tf.int32
        mask_placeholder:  Mask placeholder tensor of shape (None, self.max_length), type tf.bool
        dropout_placeholder: Dropout value placeholder (scalar), type tf.float32

        """
        self.input_placeholder = tf.placeholder(tf.int32, shape = (None, self.max_length))
        self.labels_placeholder = tf.placeholder(tf.int32, shape = (None, self.max_length))
        self.mask_placeholder = tf.placeholder(tf.bool, shape = (None, self.max_length))
        self.dropout_placeholder = tf.placeholder(tf.float32, shape = ())

    def create_feed_dict(self, inputs_batch, mask_batch, labels_batch=None, dropout=1):
        """Creates the feed_dict for the dependency parser.

        A feed_dict takes the form of:

        feed_dict = {
                <placeholder>: <tensor of values to be passed for placeholder>,
                ....
        }

        Args:
            inputs_batch: A batch of input data.
            mask_batch:   A batch of mask data.
            labels_batch: A batch of label data.
            dropout: The dropout rate.
        Returns:
            feed_dict: The feed dictionary mapping from placeholders to values.
        """
        feed_dict = {}
        if inputs_batch is not None:
            feed_dict[self.input_placeholder] = inputs_batch
        if labels_batch is not None:
            feed_dict[self.labels_placeholder] = labels_batch
        if mask_batch is not None:
            feed_dict[self.mask_placeholder] = mask_batch
        if dropout is not None:
            feed_dict[self.dropout_placeholder] = dropout
        return feed_dict

    def add_embedding(self):
        """Adds an embedding layer that maps from input tokens (integers) to vectors and then
        concatenates those vectors:

            - Create an embedding tensor and initialize it with self.pretrained_embeddings.
            - Use the input_placeholder to index into the embeddings tensor, resulting in a
              tensor of shape (None, max_length, n_features, embedding_size).
            - Concatenates the embeddings by reshaping the embeddings tensor to shape
              (None, max_length, n_features * embedding_size).

        Returns:
            embeddings: tf.Tensor of shape (None, n_features*embed_size)
        """
        embeddings = tf.Variable(self.pretrained_embeddings)
        embeddings = tf.nn.embedding_lookup(embeddings,self.input_placeholder)
        embeddings = tf.reshape(embeddings,(-1,self.max_length,Config.embed_size))
        return embeddings

    def add_prediction_op(self):
        """Adds the unrolled RNN:
            h_0 = 0
            for t in 1 to T:
                o_t, h_t = cell(x_t, h_{t-1})
                o_drop_t = Dropout(o_t, dropout_rate)
                y_t = o_drop_t U + b_2

        TODO: There a quite a few things you'll need to do in this function:
            - Define the variables U, b_2.
            - Define the vector h as a constant and inititalize it with
              zeros. See tf.zeros and tf.shape for information on how
              to initialize this variable to be of the right shape.
              https://www.tensorflow.org/api_docs/python/constant_op/constant_value_tensors#zeros
              https://www.tensorflow.org/api_docs/python/array_ops/shapes_and_shaping#shape
            - In a for loop, begin to unroll the RNN sequence. Collect
              the predictions in a list.
            - When unrolling the loop, from the second iteration
              onwards, you will HAVE to call
              tf.get_variable_scope().reuse_variables() so that you do
              not create new variables in the RNN cell.
              See https://www.tensorflow.org/versions/master/how_tos/variable_scope/
            - Concatenate and reshape the predictions into a predictions
              tensor.
        Hint: You will find the function tf.pack (similar to np.asarray)
              useful to assemble a list of tensors into a larger tensor.
              https://www.tensorflow.org/api_docs/python/array_ops/slicing_and_joining#pack
        Hint: You will find the function tf.transpose and the perms
              argument useful to shuffle the indices of the tensor.
              https://www.tensorflow.org/api_docs/python/array_ops/slicing_and_joining#transpose

        Remember:
            * Use the xavier initilization for matrices.
            * Note that tf.nn.dropout takes the keep probability (1 - p_drop) as an argument.
            The keep probability should be set to the value of self.dropout_placeholder

        Returns:
            pred: tf.Tensor of shape (batch_size, max_length, n_classes)
        """

        x = self.add_embedding()
        dropout_rate = self.dropout_placeholder

        preds = [] # Predicted output at each timestep should go here!

        lstm = tf.nn.rnn_cell.BasicLSTMCell(Config.hidden_size)

        # Define U and b2 as variables.
        # Initialize state as vector of zeros.
        xavier_initializer = tf.contrib.layers.xavier_initializer()
        U = tf.Variable(xavier_initializer((Config.hidden_size,Config.n_classes)))
        b2 = tf.Variable(tf.zeros((Config.n_classes,)))
        #h_t = tf.Variable(tf.zeros((Config.hidden_size,)))

        with tf.variable_scope("RNN"):
	    outputs, state = tf.nn.dynamic_rnn(lstm, x, dtype=tf.float32)
	for t in range(self.max_length):
	    h_drop = tf.nn.dropout(outputs[:,t,:], dropout_rate)
            y_t = tf.matmul(h_drop,U)+b2
            preds.append(y_t)
        preds = tf.pack(preds,1)	
	assert preds.get_shape().as_list() == [None, self.max_length, Config.n_classes], "predictions are not of the right shape. Expected {}, got {}".format([None, self.max_length, Config.n_classes], preds.get_shape().as_list())
	return preds


    def add_loss_op(self, preds):
        """Adds Ops for the loss function to the computational graph.

        TODO: Compute averaged cross entropy loss for the predictions.
        Importantly, you must ignore the loss for any masked tokens.

        Hint: You might find tf.boolean_mask useful to mask the losses on masked tokens.
        Hint: You can use tf.nn.sparse_softmax_cross_entropy_with_logits to simplify your
                    implementation. You might find tf.reduce_mean useful.
        Args:
            pred: A tensor of shape (batch_size, max_length, n_classes) containing the output of the neural
                  network before the softmax layer.
        Returns:
            loss: A 0-d tensor (scalar)
        """
        
	preds_masked = tf.boolean_mask(preds,self.mask_placeholder)
        labels_masked = tf.boolean_mask(self.labels_placeholder,self.mask_placeholder)
        ce = tf.nn.sparse_softmax_cross_entropy_with_logits(preds_masked, labels_masked)
        loss = tf.reduce_mean(ce)
        return loss

    def add_training_op(self, loss):
        """Sets up the training Ops.

        Creates an optimizer and applies the gradients to all trainable variables.
        The Op returned by this function is what must be passed to the
        `sess.run()` call to cause the model to train. See

        https://www.tensorflow.org/versions/r0.7/api_docs/python/train.html#Optimizer

        for more information.

        Use tf.train.AdamOptimizer for this model.
        Calling optimizer.minimize() will return a train_op object.

        Args:
            loss: Loss tensor, from cross_entropy_loss.
        Returns:
            train_op: The Op for training.
        """
        train_op = tf.train.AdamOptimizer(Config.lr).minimize(loss)
        return train_op


    def predict_on_batch(self, sess, inputs_batch):
        feed = self.create_feed_dict(inputs_batch=inputs_batch)
        predictions = sess.run(tf.argmax(self.pred, axis=2), feed_dict=feed)
        return predictions

    def train_on_batch(self, sess, inputs_batch, labels_batch, mask_batch):
        feed = self.create_feed_dict(inputs_batch, labels_batch=labels_batch, mask_batch=mask_batch,
                                     dropout=Config.dropout)
        _, loss = sess.run([self.train_op, self.loss], feed_dict=feed)
        return loss
    """
    This function generate a response sentence back from an input contex using the model   
    @inputSentence: should be an array of contex words
    @vocab: the vocab dictionary to map id back to words
    @return: the response in an array of words
    """
    def output(self, sess, inputSentence, vocab):
  	contID = mapContex2VocabIndex([inputSentece],vocab)	 
        preds = self.predict_on_batch(sess,contID)
	print preds
	return preds	
	
    def build(self):
        self.add_placeholders()
        self.pred = self.add_prediction_op()
        self.loss = self.add_loss_op(self.pred)
        self.train_op = self.add_training_op(self.loss)

    def __init__(self, config, pretrained_embeddings):
        self.max_length = config.max_length 
        self.pretrained_embeddings = pretrained_embeddings
        self.config = config
        # Defining placeholders.
        self.input_placeholder = None
        self.labels_placeholder = None
        self.mask_placeholder = None
        self.dropout_placeholder = None

        self.build()
	print("RNN model constructed!")

def do_shell():
    testMaxSeqLen = 34
    config = Config(testMaxSeqLen)
    embeddings = load_pretrained_embed()
    vocab = load_vocab_dict()
    with tf.Graph().as_default():
        print "Building model..."
        start = time.time()
        model = RNNModel(config, embeddings)
        print "took ", time.time() - start, "seconds"

        init = tf.global_variables_initializer()
        saver = tf.train.Saver()

        with tf.Session() as session:
            session.run(init)
            saver.restore(session, model.config.model_output)

            print("""Welcome to the chatbot! please type in some chat:""")
            while True:
                # Create simple REPL
                try:
                    sentence = raw_input("input> ")
		    sentence = sentence.strip().split(" ")
                    response = model.output(session,sentence,vocab)
                    print ' '.join(response)
                except EOFError:
                    print("Closing session.")
                    break


def do_train(args):
    # Set up some parameters.
    config = Config(args)
    #helper, train, dev, train_raw, dev_raw = load_and_preprocess_data(args)
    
    vocab, embeddings = loadGloVect(vocabFileDir)
    config.embed_size = embeddings.shape[1]
    helper.save(config.output_path)

    handler = logging.FileHandler(config.log_output)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
    logging.getLogger().addHandler(handler)

    report = None #Report(Config.eval_output)

    with tf.Graph().as_default():
        logger.info("Building model...",)
        start = time.time()
        model = RNNModel(helper, config, embeddings)
        logger.info("took %.2f seconds", time.time() - start)

        init = tf.global_variables_initializer()
        saver = tf.train.Saver()

        with tf.Session() as session:
            session.run(init)
            model.fit(session, saver, train, dev)
            if report:
                report.log_output(model.output(session, dev_raw))
                report.save()
            else:
                # Save predictions in a text file.
                output = model.output(session, dev_raw)
                sentences, labels, predictions = zip(*output)
                predictions = [[LBLS[l] for l in preds] for preds in predictions]
                output = zip(sentences, labels, predictions)

                with open(model.config.conll_output, 'w') as f:
                    write_conll(f, output)
                with open(model.config.eval_output, 'w') as f:
                    for sentence, labels, predictions in output:
                        print_sentence(f, sentence, labels, predictions)


if __name__ == "__main__":
    test_pad_sequences() 
    load_test_data()
    testRNNModel()
    #do_shell()
