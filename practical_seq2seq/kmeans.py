from __future__ import division
#import tensorflow as tf
import numpy as np

# preprocessed data
from datasets.twitter import data
import data_utils
import constants
import pickle
import sys
from scipy import spatial
from scipy.cluster.vq import vq, kmeans, whiten
from numpy import random

TWITTER_PRETRAINED_FILE = constants.TWITTER_PRETRAINED_FILE
VOCAB_DIMENSIONS = constants.VOCAB_DIMENSIONS

# load data from pickle and npy files
metadata, idx_q, idx_a = data.load_data(PATH='datasets/twitter/')
(trainX, trainY), (testX, testY), (validX, validY) = data_utils.split_dataset(idx_q, idx_a)

# parameters 
xseq_len = trainX.shape[-1]
yseq_len = trainY.shape[-1]
batch_size = 16
xvocab_size = len(metadata['idx2w'])  
yvocab_size = xvocab_size
emb_dim = constants.VOCAB_DIMENSIONS

import seq2seq_wrapper


# return: (tokens, average word2vec)
def getSampleVec():
  test_batch_gen = data_utils.rand_batch_gen(testX, testY, 1)
  input_ = test_batch_gen.next()[0]
  flatten = lambda l: [item for sublist in l for item in sublist]
  input_ = flatten(input_) # flattened tokens
  vec = np.zeros((len(input_), VOCAB_DIMENSIONS))
  for i, token in enumerate(input_):
    vec[i] = twitter_embed[token]
  return input_, np.average(vec, axis=0)

# return: (tokens, average word2vec)
def getAvgEmbedsMatrix(matrix):
  # Average all the test vectors
  num_rows = len(matrix)
  num_cols = len(matrix[0])
  vecs = np.zeros((num_rows, num_cols, VOCAB_DIMENSIONS))
  for r_i, row in enumerate(matrix):
    for i, token in enumerate(row):
      vecs [r_i][i] = twitter_embed[token]
  return matrix, np.average(vecs, axis=1)

# Load twitter pretrained embeddings
f = open(TWITTER_PRETRAINED_FILE, 'rb')
twitter_embed = pickle.load(f)
f.close()

# Take one random sample
sampleVec_tokens, sampleVec_embed = getSampleVec()

# Average all the test vectors
vecs_tokens, vecs_embed = getAvgEmbedsMatrix(testX)

# Find N nearest neighbour questions
N_nearest = 500
tree = spatial.KDTree(vecs_embed)
_, neighbor_idxs = tree.query(sampleVec_embed, k=N_nearest)
nearest_neighbours_tokens = np.array([vecs_tokens[idx] for idx in neighbor_idxs])

# Run questions through neural network
model = seq2seq_wrapper.Seq2Seq(xseq_len=xseq_len,
                               yseq_len=yseq_len,
                               xvocab_size=xvocab_size,
                               yvocab_size=yvocab_size,
                               ckpt_path='ckpt/',
                               emb_dim=emb_dim,
                               num_layers=3
                               )
sess = model.restore_last_session()
input_ = nearest_neighbours_tokens.T
output = model.predict(sess, input_)

def forbid_unk(oi):
  return oi[0] if oi[0] != 1 else oi[1]

output = np.apply_along_axis(forbid_unk, 2, output)

# Remove duplicate answers
def unique_rows(a):
    a = np.ascontiguousarray(a)
    unique_a = np.unique(a.view([('', a.dtype)]*a.shape[1]))
    return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))
output = unique_rows(output)

# Find k means centroids
k = 2
random.seed((1000,2000))
output_tokens, output_embeds = getAvgEmbedsMatrix(output)
whitened = whiten(output_embeds)
centroids, _ = kmeans(whitened,k)

# Find nearest answer to each centroid
print "OUTPUT EMBEDS"
print output_embeds.shape

print "CENTROIDS"
print centroids.shape

tree = spatial.KDTree(output_embeds)
replies = []
k_nearest = 3
#for i in range(10):
#  _, idxs = tree.query(output_embeds[i], k=k_nearest)
#  for idx in idxs:
#    replies.append(output_tokens[idx])

for centroid in centroids:
  _, idxs = tree.query(centroid, k=k_nearest)
  for idx in idxs:
    replies.append(output_tokens[idx])

print "Original Question: "
print data_utils.decode(sequence=sampleVec_tokens, lookup=metadata['idx2w'], separator=' ')

print "Nearest Answers: "
for idx, o in enumerate(replies):
  if idx and idx % k_nearest == 0:
    print '+++++++++++++++++++++++++++'
  print data_utils.decode(sequence=o, lookup=metadata['idx2w'], separator=' ')

print "Possible Answers: "
for o in output:
  print data_utils.decode(sequence=o, lookup=metadata['idx2w'], separator=' ')




