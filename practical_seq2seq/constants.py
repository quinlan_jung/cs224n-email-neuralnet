# 1/ python dataPreprocess.py
# 2/ python datasets/twitter/data.py
# 3/ python make_embeddings.py
# 4/ python 03-Twitter-chatbot.py
# 5/ python test.py

VOCAB_DIMENSIONS = 300
TWITTER_VOCAB_SIZE = 8000
PRETRAINED_FILE = 'pretrained_embed-' + str(VOCAB_DIMENSIONS) + '.pckl'
VOCAB_FILE = 'vocab-' + str(VOCAB_DIMENSIONS) + '.pckl'
TWITTER_PRETRAINED_FILE = 'pretrained_twittEmbed-'+str(VOCAB_DIMENSIONS)+'D-'+str(TWITTER_VOCAB_SIZE)+'N'+'.pckl'
SEMANTIC_CLUSTER_INPUT = 'sic_in.pckl'
