import numpy as np
import sys
import pickle
import constants

VOCAB_DIMENSIONS = constants.VOCAB_DIMENSIONS
TWITTER_VOCAB_SIZE = constants.TWITTER_VOCAB_SIZE
TWITTER_PRETRAINED_FILE = constants.TWITTER_PRETRAINED_FILE
PRETRAINED_FILE = constants.PRETRAINED_FILE
VOCAB_FILE = constants.VOCAB_FILE

def load_pretrained_embed():
    f = open(TWITTER_PRETRAINED_FILE, 'rb')
    embed = pickle.load(f)
    f.close()
    return embed 

def make_pretrained_embed(vocab,embedSize):
    print "Loading Pretrained Glove File."
    f = open(PRETRAINED_FILE, 'rb')
    embed = pickle.load(f)
    f.close()
    print "Loading Twitter Metadata."
    f = open('datasets/twitter/metadata.pkl')
    metadata = pickle.load(f)
    f.close()
    twitterVocab_w2idx = metadata['w2idx']
    TWITTER_VOCAB_SIZE_PLUS2 = len(twitterVocab_w2idx)
    twitterEmbed = np.array([[0.0]*embedSize for i in range(TWITTER_VOCAB_SIZE_PLUS2)])

    for k,v in twitterVocab_w2idx.items():
        if k in vocab:
            twitterEmbed[v] = embed[vocab[k]]
    with open(TWITTER_PRETRAINED_FILE,'wb') as f:
        pickle.dump(twitterEmbed,f)

def load_vocab_dict():
    print "Loading Vocabulary."
    f = open(VOCAB_FILE,'rb')
    vocab = pickle.load(f)
    f.close()
    return vocab

if __name__ == "__main__":
    vocab = load_vocab_dict()
    make_pretrained_embed(vocab, VOCAB_DIMENSIONS)
    embed =  load_pretrained_embed()
    print embed[0:4]