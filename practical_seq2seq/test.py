import tensorflow as tf
import numpy as np

# preprocessed data
from datasets.twitter import data
import data_utils
import constants
import pickle

# load data from pickle and npy files
metadata, idx_q, idx_a = data.load_data(PATH='datasets/twitter/')
(trainX, trainY), (testX, testY), (validX, validY) = data_utils.split_dataset(idx_q, idx_a)

# parameters 
xseq_len = trainX.shape[-1]
yseq_len = trainY.shape[-1]
batch_size = 16
xvocab_size = len(metadata['idx2w'])  
yvocab_size = xvocab_size
emb_dim = constants.VOCAB_DIMENSIONS
SEMANTIC_CLUSTER_INPUT = constants.SEMANTIC_CLUSTER_INPUT

import seq2seq_wrapper

model = seq2seq_wrapper.Seq2Seq(xseq_len=xseq_len,
                               yseq_len=yseq_len,
                               xvocab_size=xvocab_size,
                               yvocab_size=yvocab_size,
                               ckpt_path='ckpt/',
                               emb_dim=emb_dim,
                               num_layers=3
                               )

test_batch_gen = data_utils.rand_batch_gen(testX, testY, 256)

sess = model.restore_last_session()

input_ = test_batch_gen.next()[0]
output = model.predict(sess, input_)

replies = set()
for ii, oi in zip(input_.T, output):
    q = data_utils.decode(sequence=ii, lookup=metadata['idx2w'], separator=' ')
    oi = [ o_list[0] if o_list[0] != 1 else o_list[1] for o_list in oi] # Forbid UNK from showing up in testing
    decoded = data_utils.decode(sequence=oi, lookup=metadata['idx2w'], separator=' ')
    print q
    print decoded
    replies.add(decoded)

# Save the outputs
f = open(SEMANTIC_CLUSTER_INPUT,'wb')
pickle.dump(replies,f)
f.close()




